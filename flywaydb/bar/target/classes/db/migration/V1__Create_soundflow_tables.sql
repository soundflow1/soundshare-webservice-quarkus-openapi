create schema sampleservice;

create table sampleservice.samples (
    sample_id int not null PRIMARY KEY ,
    user_id int,
    username varchar(100) not null,
    sample_name varchar(100) not null,
    sample_location varchar(100) not null,
    sample_length varchar(100) not null,
    sample_genre varchar(100),
    sample_private boolean not null
);
create schema userservice;

create table userservice.users (
    user_id int not null PRIMARY KEY,
    keycloak_id int,
    username varchar(100) not null unique,
    name varchar(100) not null,
    last_name varchar(100) not null,
    artist_name varchar(100) not null,
    email varchar(100) not null
);

create schema socialservice;

create table socialservice.comments (
    comment_id int not null PRIMARY KEY,
    sample_id int not null,
    username varchar(100) not null,
    content varchar(100) not null
);

create table socialservice.statistics (
    statistic_id int not null PRIMARY KEY,
    sample_id int not null,
    like_count int not null,
    plays_count int not null,
    comments_count int not null
);

create table socialservice.friendlist (
    friendlist_id int not null PRIMARY KEY,
    username varchar(100) not null,
    friend_id int[]
);

