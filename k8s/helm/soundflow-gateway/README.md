### Configuration

Helm values to define for backoffice-gateway:

| Parameter  | Description      | Default |
| ---------- | ---------------  | ------- |
| `urls.hostname` | Hostname where current instance is hosted on | `api.powerprice.marstan.net` |
