package com.powerprice.apigateway.model;

import java.time.LocalDateTime;

public class Sample {
  public Long id;
  public String username;
  public String name;
  public int length;
  public String sampleLocation;
  public String genre;
  public boolean hidden;
  public String imageLocation;
  public LocalDateTime creationDate;

  public Sample() {}

  public Sample(
      Long id,
      String username,
      String name,
      String sampleLocation,
      String genre,
      Boolean hidden,
      int length,
      String imageLocation,
      LocalDateTime creationDate) {
    this.id = id;
    this.username = username;
    this.name = name;
    this.sampleLocation = sampleLocation;
    this.genre = genre;
    this.hidden = hidden;
    this.length = length;
    this.imageLocation = imageLocation;
    this.creationDate = creationDate;
  }

  public Sample(
      String username,
      String name,
      String location,
      String genre,
      Boolean hidden,
      int length,
      String imageLocation,
      LocalDateTime creationDate) {
    this.username = username;
    this.name = name;
    this.sampleLocation = location;
    this.genre = genre;
    this.hidden = hidden;
    this.length = length;
    this.imageLocation = imageLocation;
    this.creationDate = creationDate;
  }
}
