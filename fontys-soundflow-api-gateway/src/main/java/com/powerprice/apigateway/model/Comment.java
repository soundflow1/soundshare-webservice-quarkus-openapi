package com.powerprice.apigateway.model;

public class Comment {

  public Long id;
  public String username;
  public String content;
  public Long sampleId;

  public Comment() {}

  public Comment(Long id, String username, String content, Long sampleId) {
    this.id = id;
    this.username = username;
    this.content = content;
    this.sampleId = sampleId;
  }

  public Comment(Long sampleId, String username, String content) {
    this.sampleId = sampleId;
    this.username = username;
    this.content = content;
  }
}
