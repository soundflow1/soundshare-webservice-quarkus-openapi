package com.powerprice.apigateway.services;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class StatisticService {
  @Value("${gw.statisticServiceUrl}")
  private String statisticServiceUrl;

  private WebClient webClient;

  @PostConstruct
  private void init() {
    this.webClient =
        WebClient.builder()
            .baseUrl(statisticServiceUrl)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, "application/json")
            .build();
  }

  public ResponseEntity<String> get(Long id) {
    return webClient
        .get()
        .uri("/statistics/" + id)
        .exchange()
        .flatMap(response -> response.toEntity(String.class))
        .block();
  }

  public ResponseEntity<String> getBySample(Long id) {
    return webClient
        .get()
        .uri("/statistics/bysample/" + id)
        .exchange()
        .flatMap(response -> response.toEntity(String.class))
        .block();
  }

  public ResponseEntity<String> get() {
    return webClient
        .get()
        .uri("/statistics/")
        .exchange()
        .flatMap(response -> response.toEntity(String.class))
        .block();
  }

  public ResponseEntity<String> delete(Long id) {
    return webClient
        .delete()
        .uri("/statistics/" + id)
        .exchange()
        .flatMap(response -> response.toEntity(String.class))
        .block();
  }

  public ResponseEntity<String> updateLikes(Long sampleId) {
    return webClient
        .post()
        .uri("/statistics/likes/" + sampleId)
        .exchange()
        .flatMap(response -> response.toEntity(String.class))
        .block();
  }

  public ResponseEntity<String> updatePlays(Long sampleId) {
    return webClient
        .post()
        .uri("/statistics/plays/" + sampleId)
        .exchange()
        .flatMap(response -> response.toEntity(String.class))
        .block();
  }
}
