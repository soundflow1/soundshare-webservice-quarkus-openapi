package com.powerprice.apigateway.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Service
public class SampleService {
  @Value("${gw.sampleServiceUrl}")
  private String sampleServiceUrl;

  private WebClient webClient;

  @PostConstruct
  private void init() {
    this.webClient =
        WebClient.builder()
            .baseUrl(sampleServiceUrl)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, "application/json")
            .build();
  }

  public ResponseEntity<String> get() {
    return webClient
        .get()
        .uri("/samples/")
        .exchange()
        .flatMap(response -> response.toEntity(String.class))
        .block();
  }

  public ResponseEntity<String> get(Long id) {
    return webClient
        .get()
        .uri("/samples/" + id)
        .exchange()
        .flatMap(response -> response.toEntity(String.class))
        .block();
  }

  public ResponseEntity<String> getByUser(String username) {
    return webClient
        .get()
        .uri("/samples/byuser/" + username)
        .exchange()
        .flatMap(response -> response.toEntity(String.class))
        .block();
  }

  public ResponseEntity<String> delete(Long id) {
    return webClient
        .delete()
        .uri("/samples/" + id)
        .exchange()
        .flatMap(response -> response.toEntity(String.class))
        .block();
  }

  public ResponseEntity<String> create(
      MultipartFile file,
      MultipartFile imageFile,
      String fileName,
      String username,
      String genre,
      Boolean sampleHidden,
      int sampleLength) {
    MultiValueMap<String, Object> mvm = new LinkedMultiValueMap<>();

    mvm.add("file", new FileSystemResource(convert(file)));
    mvm.add("imageFile", new FileSystemResource(convert(imageFile)));
    mvm.add("fileName", fileName);
    mvm.add("sampleUsername", username);
    mvm.add("sampleGenre", genre);
    mvm.add("sampleHidden", sampleHidden);
    mvm.add("sampleLength", sampleLength);
    return webClient
        .post()
        .uri("/samples/")
        .body(BodyInserters.fromMultipartData(mvm))
        .exchange()
        .flatMap(response -> response.toEntity(String.class))
        .block();
  }

  public static File convert(MultipartFile file) {
    File convFile = new File(file.getOriginalFilename());
    try {
      convFile.createNewFile();
      FileOutputStream fos = new FileOutputStream(convFile);
      fos.write(file.getBytes());
      fos.close();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return convFile;
  }
}
