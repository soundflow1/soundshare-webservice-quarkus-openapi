package com.powerprice.apigateway.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import javax.annotation.PostConstruct;

@Service
public class CommentService {
  @Value("${gw.commentServiceUrl}")
  private String commentServiceUrl;

  private WebClient webClient;

  @PostConstruct
  private void init() {
    this.webClient =
        WebClient.builder()
            .baseUrl(commentServiceUrl)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, "application/json")
            .build();
  }

  public ResponseEntity<String> get(Long id) {
    return webClient
        .get()
        .uri("/statistics/" + id)
        .exchange()
        .flatMap(response -> response.toEntity(String.class))
        .block();
  }

  public ResponseEntity<String> get() {
    return webClient
        .get()
        .uri("/statistics/")
        .exchange()
        .flatMap(response -> response.toEntity(String.class))
        .block();
  }

  public ResponseEntity<String> delete(Long id) {
    return webClient
        .delete()
        .uri("/statistics/" + id)
        .exchange()
        .flatMap(response -> response.toEntity(String.class))
        .block();
  }

  public ResponseEntity<String> create(Long sampleId, String username, String content) {
    MultiValueMap<String, Object> mvm = new LinkedMultiValueMap<>();
    mvm.add("sample_id", sampleId);
    mvm.add("username", username);
    mvm.add("content", content);
    return webClient
        .post()
        .uri("/statistics/")
        .body(BodyInserters.fromMultipartData(mvm))
        .exchange()
        .flatMap(response -> response.toEntity(String.class))
        .block();
  }
}
