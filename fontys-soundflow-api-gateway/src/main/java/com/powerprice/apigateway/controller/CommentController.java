package com.powerprice.apigateway.controller;

import com.powerprice.apigateway.services.CommentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("comments")
public class CommentController {

  private CommentService commentService;

  public CommentController(CommentService commentService) {
    this.commentService = commentService;
  }

  @GetMapping("{id}")
  public ResponseEntity<String> get(@PathVariable(value = "id") Long id) {
    return commentService.get(id);
  }

  @GetMapping
  public ResponseEntity<String> get() {
    return commentService.get();
  }

  @PostMapping
  public ResponseEntity<String> create(
      @RequestParam("sample_id") Long sampleId,
      @RequestParam("username") String username,
      @RequestParam("content") String content) {
    return commentService.create(sampleId, username, content);
  }

  @DeleteMapping("{id}")
  public ResponseEntity<String> delete(@PathVariable(value = "id") Long id) {
    return commentService.delete(id);
  }
}
