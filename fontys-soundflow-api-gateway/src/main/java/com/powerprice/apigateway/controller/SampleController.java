package com.powerprice.apigateway.controller;

import com.powerprice.apigateway.services.SampleService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("samples")
public class SampleController {

  private SampleService sampleService;

  public SampleController(SampleService sampleService) {
    this.sampleService = sampleService;
  }

  @GetMapping("{id}")
  public ResponseEntity<String> get(@PathVariable(value = "id") Long id) {
    return sampleService.get(id);
  }

  @GetMapping
  public ResponseEntity<String> get() {
    return sampleService.get();
  }

  // dit werkt nog niet!
  @GetMapping("/byuser/{username}")
  public ResponseEntity<String> getByUser(@PathVariable(value = "username") String username) {
    return sampleService.getByUser(username);
  }

  @PostMapping
  public ResponseEntity<String> create(
      @RequestParam("file") MultipartFile file,
      @RequestParam("imageFile") MultipartFile imageFile,
      @RequestParam("fileName") String fileName,
      @RequestParam("sampleUsername") String username,
      @RequestParam("sampleGenre") String genre,
      @RequestParam("sampleHidden") Boolean sampleHidden,
      @RequestParam("sampleLength") int sampleLength) {
    return sampleService.create(
        file, imageFile, fileName, username, genre, sampleHidden, sampleLength);
  }

  @DeleteMapping("{id}")
  public ResponseEntity<String> delete(@PathVariable(value = "id") Long id) {
    return sampleService.delete(id);
  }
}
