package com.powerprice.apigateway.controller;

import com.powerprice.apigateway.services.StatisticService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("statistics")
public class StatisticController {
  private StatisticService statisticService;

  public StatisticController(StatisticService statisticService) {
    this.statisticService = statisticService;
  }

  @GetMapping("{id}")
  public ResponseEntity<String> get(@PathVariable(value = "id") Long id) {
    return statisticService.get(id);
  }

  @GetMapping
  public ResponseEntity<String> get() {
    return statisticService.get();
  }

  @GetMapping("bysample/{sample_id}")
  public ResponseEntity<String> getSingleBySample(@PathVariable Long sample_id) {
    return statisticService.getBySample(sample_id);
  }
  // this is not updating in the frontend because play count is 0 to begin with maybe insert 0
  @PostMapping("plays/{sample_id}")
  public ResponseEntity<String> updatePlays(@PathVariable Long sample_id) {
    return statisticService.updatePlays(sample_id);
  }

  @PostMapping("likes/{sample_id}")
  public ResponseEntity<String> updateLikes(@PathVariable Long sample_id) {
    return statisticService.updateLikes(sample_id);
  }

  @DeleteMapping("{id}")
  public ResponseEntity<String> delete(@PathVariable(value = "id") Long id) {
    return statisticService.delete(id);
  }
}
