
create table sampleservice.samples (
    sample_id serial PRIMARY KEY,
    username varchar(100) not null,
    sample_name varchar(100) not null,
    sample_location varchar(100) not null,
    sample_length int,
    sample_genre varchar(100) not null,
    sample_hidden boolean not null,
    sample_location_image varchar(100) not null,
    sample_date_creation timestamp not null
);

create table userservice.users (
    user_id serial PRIMARY KEY,
    keycloak_id int,
    username varchar(100) not null unique,
    name varchar(100) not null,
    last_name varchar(100) not null,
    artist_name varchar(100) not null,
    email varchar(100) not null
);

create table socialservice.comments (
    comment_id serial PRIMARY KEY,
    sample_id int not null,
    username varchar(100) not null,
    content varchar(100) not null
);

create table socialservice.statistics (
    statistic_id serial PRIMARY KEY,
    sample_id int not null unique,
    like_count int,
    play_count int
);

create table socialservice.friendlist (
    friendlist_id serial PRIMARY KEY,
    username varchar(100) not null,
    friend_id int[]
);


