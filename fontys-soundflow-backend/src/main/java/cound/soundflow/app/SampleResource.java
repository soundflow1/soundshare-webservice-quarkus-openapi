package cound.soundflow.app;

import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import java.net.URI;
import java.time.LocalDateTime;

@Path("/samples")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SampleResource {

    @Inject
    PgPool client;

    @GET
    public Uni<Response> get() {
        return Sample.findAll(client)
                .onItem().apply(Response::ok)
                .onItem().apply(ResponseBuilder::build);
    }

    @GET
    @Path("{id}")
    public Uni<Response> getSingle(@PathParam Long id) {
        return Sample.findById(client, id)
                .onItem().apply(sample -> sample != null ? Response.ok(sample) : Response.status(Status.NOT_FOUND))
                .onItem().apply(ResponseBuilder::build);
    }

    @GET
    @Path("/byuser/{username}")
    public Uni<Response> getSingle(@PathParam String username) {
        return Sample.findByUsername(client, username)
                .onItem().apply(Response::ok)
                .onItem().apply(ResponseBuilder::build);
    }


    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Uni<Response> create(@MultipartForm MultipartBody data) {
        Sample s = new Sample(data.sampleUsername, data.fileName,  BucketService.uploadSample(data.file, data.fileName).toString(),
                data.sampleGenre, data.sampleHidden, data.sampleLength, BucketService.uploadImage(data.imageFile, data.fileName).toString(), LocalDateTime.now());
        return s.save(client)
                .onItem().apply(id -> URI.create("/samples/" + id))
                .onItem().apply(uri -> Response.created(uri).build());
    }

    @PUT
    @Path("{id}")
    public Uni<Response> update(@PathParam Long id, Sample sample) {
        return sample.update(client)
                .onItem().apply(updated -> updated ? Status.OK : Status.NOT_FOUND)
                .onItem().apply(status -> Response.status(status).build());
    }

    @DELETE
    @Path("{id}")
    public Uni<Response> delete(@PathParam Long id) {
        return Sample.delete(client, id)
                .onItem().apply(deleted -> deleted ? Status.NO_CONTENT : Status.NOT_FOUND)
                .onItem().apply(status -> Response.status(status).build());
    }
}