package cound.soundflow.app;

import org.jboss.resteasy.annotations.providers.multipart.PartType;

import javax.ws.rs.FormParam;
import javax.ws.rs.core.MediaType;
import java.io.InputStream;

public class MultipartBody {


    @FormParam("file")
    @PartType(MediaType.APPLICATION_OCTET_STREAM)
    public InputStream file;

    @FormParam("imageFile")
    @PartType(MediaType.APPLICATION_OCTET_STREAM)
    public InputStream imageFile;

    @FormParam("fileName")
    @PartType(MediaType.TEXT_PLAIN)
    public String fileName;

    @FormParam("sampleUsername")
    @PartType(MediaType.TEXT_PLAIN)
    public String sampleUsername;

    @FormParam("sampleGenre")
    @PartType(MediaType.TEXT_PLAIN)
    public String sampleGenre;

    @FormParam("sampleHidden")
    @PartType(MediaType.TEXT_PLAIN)
    public boolean sampleHidden;

    @FormParam("sampleLength")
    @PartType(MediaType.TEXT_PLAIN)
    public int sampleLength;

}