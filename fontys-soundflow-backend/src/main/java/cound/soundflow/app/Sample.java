package cound.soundflow.app;

import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.RowSet;
import io.vertx.mutiny.sqlclient.Tuple;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Sample {
    public Long id;
    public String username;
    public String name;
    public int length;
    public String sampleLocation;
    public String genre;
    public boolean hidden;
    public String imageLocation;
    public LocalDateTime creationDate;

    public Sample() {
    }

    public Sample(Long id, String username, String name, String sampleLocation, String genre, Boolean hidden, int length, String imageLocation, LocalDateTime creationDate) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.sampleLocation = sampleLocation;
        this.genre = genre;
        this.hidden = hidden;
        this.length = length;
        this.imageLocation = imageLocation;
        this.creationDate = creationDate;
    }

    public Sample(String username, String name, String location, String genre, Boolean hidden, int length, String imageLocation, LocalDateTime creationDate) {
        this.username = username;
        this.name = name;
        this.sampleLocation = location;
        this.genre = genre;
        this.hidden = hidden;
        this.length = length;
        this.imageLocation = imageLocation;
        this.creationDate = creationDate;
    }

    public static Uni<List<Sample>> findAll(PgPool client) {
        return client.query("select sample_id, username, sample_name, sample_location, " +
                "sample_length, sample_genre, sample_hidden, sample_location_image, sample_date_creation from sampleservice.samples")
                .onItem().apply(pgRowSet -> {
                    List<Sample> list = new ArrayList<>(pgRowSet.size());
                    for (Row row : pgRowSet) {
                        list.add(from(row));
                    }
                    return list;
                });
    }

    public static Uni<Sample> findById(PgPool client, Long id) {
        return client.preparedQuery("SELECT sample_id, username, sample_name, sample_location, " +
                "sample_length, sample_genre, sample_hidden, sample_location_image, sample_date_creation FROM sampleservice.samples WHERE sample_id = $1", Tuple.of(id))
                .onItem().apply(RowSet::iterator)
                .onItem().apply(iterator -> iterator.hasNext() ? from(iterator.next()) : null);
    }

    public static Uni<List<Sample>> findByUsername(PgPool client, String username) {
        return client.preparedQuery("SELECT sample_id, username, sample_name, sample_location, " +
                "sample_length, sample_genre, sample_hidden, sample_location_image, sample_date_creation FROM sampleservice.samples WHERE username = $1",  Tuple.of(username))
                .onItem().apply(pgRowSet -> {
                    List<Sample> list = new ArrayList<>(pgRowSet.size());
                    for (Row row : pgRowSet) {
                        list.add(from(row));
                    }
                    return list;
                });
    }

    public Uni<Long> save(PgPool client) {
        List<Object> sampleList = new ArrayList<>();
        sampleList.add(name);
        sampleList.add(username);
        sampleList.add(sampleLocation);
        sampleList.add(length);
        sampleList.add(genre);
        sampleList.add(hidden);
        sampleList.add(imageLocation);
        sampleList.add(creationDate);
        return client.preparedQuery("INSERT INTO sampleservice.samples (sample_name, username, sample_location, sample_length, sample_genre, sample_hidden, sample_location_image, sample_date_creation) " +
                "VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING (sample_id)", Tuple.wrap(sampleList))
                .onItem().apply(pgRowSet -> pgRowSet.iterator().next().getLong("id"));
    }

    public Uni<Boolean> update(PgPool client) {
        return client.preparedQuery("UPDATE sampleservice.samples SET sample_name = $1 WHERE sample_id = $2", Tuple.of(name, id))
                .onItem().apply(pgRowSet -> pgRowSet.rowCount() == 1);
    }

    public static Uni<Boolean> delete(PgPool client, Long id) {
        return client.preparedQuery("DELETE FROM sampleservice.samples WHERE sample_id = $1", Tuple.of(id))
                .onItem().apply(pgRowSet -> pgRowSet.rowCount() == 1);
    }

    private static Sample from(Row row)  {
            return new Sample(row.getLong("sample_id"), row.getString("username"),
                    row.getString("sample_name"), row.getString("sample_location"),row.getString("sample_genre"),
                    row.getBoolean("sample_hidden"), row.getInteger("sample_length"), row.getString("sample_location_image"),
                    row.getLocalDateTime("sample_date_creation"));
    }

}