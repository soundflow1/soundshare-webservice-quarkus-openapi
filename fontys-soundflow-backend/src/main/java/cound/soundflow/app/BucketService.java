package cound.soundflow.app;

import com.amazonaws.SdkClientException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

import java.io.InputStream;
import java.net.URL;

public class BucketService {

    private static String sampleBucketName = "soundflowsamples";
    private static String imageBucketName = "soundflowsamplesimages";
    private static Regions clientRegion = Regions.US_EAST_1;

    private static AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
            .withRegion(clientRegion).build();

    public static URL uploadSample(InputStream inputStream, String fileName) {
        URL uri = null;
        try {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("audio/mpeg");
            metadata.addUserMetadata("title", "someTitle");
            PutObjectRequest request = new PutObjectRequest(sampleBucketName, fileName, inputStream, metadata);
            request.withCannedAcl(CannedAccessControlList.PublicReadWrite);
            s3Client.putObject(request);
            uri = s3Client.getUrl(sampleBucketName, fileName);
        } catch (SdkClientException e) {
            e.printStackTrace();
        }
        return uri;
    }

    public static URL uploadImage(InputStream inputStream, String fileName) {
        URL uri = null;
        try {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("image/jpeg");
            metadata.addUserMetadata("title", "someTitle");
            PutObjectRequest request = new PutObjectRequest(imageBucketName, fileName, inputStream, metadata);
            request.withCannedAcl(CannedAccessControlList.PublicReadWrite);
            s3Client.putObject(request);
            uri = s3Client.getUrl(imageBucketName, fileName);
        } catch (SdkClientException e) {
            e.printStackTrace();
        }
        return uri;
    }

    public static void deleteFile(String key, String bucketName) {
        try {
            s3Client.deleteObject(bucketName, key);
        } catch (SdkClientException e) {
            e.printStackTrace();
        }
    }

}
