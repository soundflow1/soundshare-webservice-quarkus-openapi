# soundshare-webservice-vertx-openapi

### Technology Stack

Component         | Technology
---               | ---
Backend (REST)    | [Vertx](https://vertx.io/) (Java)
Security          | Token Based (Keycloak and [JWT](https://github.com/auth0/java-jwt) )
REST Documentation| [Swagger UI / Springfox](https://github.com/springfox/springfox) and [ReDoc](https://github.com/Rebilly/ReDoc)
REST Spec         | [Open API Standard](https://www.openapis.org/)
Databases         | PostgreSQL
Server Build Tools| Maven(Java)


### Accessing Application
Component         | URL                                      
---               | ---                                      
Keycloak          |  http://localhost:8080 
Backend           |  http://localhost:9000                   
Database          |    
Swagger (API Ref) |  http://localhost:8080/swagger-ui.html  