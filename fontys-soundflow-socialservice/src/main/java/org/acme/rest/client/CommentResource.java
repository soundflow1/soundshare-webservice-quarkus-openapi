package org.acme.rest.client;

import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/statistics")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CommentResource {

    @Inject
    PgPool client;

    @GET
    public Uni<Response> get() {
        return Comment.findAll(client)
                .onItem().apply(Response::ok)
                .onItem().apply(Response.ResponseBuilder::build);
    }

    //get by sampleid not commentid
    @GET
    @Path("{id}")
    public Uni<Response> getSingle(@PathParam Long id) {
        return Comment.findById(client, id)
                .onItem().apply(comment -> comment != null ? Response.ok(comment) : Response.status(Response.Status.NOT_FOUND))
                .onItem().apply(Response.ResponseBuilder::build);
    }

//    @GET
//    @Path("{username}")
//    public Uni<Response> getSingle(@PathParam String username) {
//        return Comment.findByUsername(client, username)
//                .onItem().apply(Response::ok)
//                .onItem().apply(Response.ResponseBuilder::build);
//    }


    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    public Uni<Response> create(@MultipartForm MultipartBody data) {
        Comment c = new Comment(data.sampleId, data.username, data.content);
        return c.save(client)
                .onItem().apply(id -> URI.create("/comments/" + id))
                .onItem().apply(uri -> Response.created(uri).build());
    }

    @PUT
    @Path("{id}")
    public Uni<Response> update(@PathParam Long id, Comment comment) {
        return comment.update(client)
                .onItem().apply(updated -> updated ? Response.Status.OK : Response.Status.NOT_FOUND)
                .onItem().apply(status -> Response.status(status).build());
    }

    @DELETE
    @Path("{id}")
    public Uni<Response> delete(@PathParam Long id) {
        return Comment.delete(client, id)
                .onItem().apply(deleted -> deleted ? Response.Status.NO_CONTENT : Response.Status.NOT_FOUND)
                .onItem().apply(status -> Response.status(status).build());
    }
}
