package org.acme.rest.client;

import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.Tuple;

import java.util.ArrayList;
import java.util.List;

public class Comment {
    public Long id;
    public String username;
    public String content;
    public Long sampleId;

    public Comment() {

    }

    public Comment(Long id, String username, String content, Long sampleId) {
        this.id = id;
        this.username = username;
        this.content = content;
        this.sampleId = sampleId;
    }

    public Comment(Long sampleId, String username, String content) {
        this.sampleId = sampleId;
        this.username = username;
        this.content = content;
    }

    public static Uni<List<Comment>> findAll(PgPool client) {
        return client.query("select comment_id, sample_id, username, content from socialservice.comments")
                .onItem().apply(pgRowSet -> {
                    List<Comment> list = new ArrayList<>(pgRowSet.size());
                    for (Row row : pgRowSet) {
                        list.add(from(row));
                    }
                    return list;
                });
    }

    public static Uni<List<Comment>> findById(PgPool client, Long id) {
        return client.preparedQuery("SELECT comment_id, sample_id, username, content FROM socialservice.comments WHERE sample_id = $1", Tuple.of(id))
                .onItem().apply(pgRowSet -> {
                    List<Comment> list = new ArrayList<>(pgRowSet.size());
                    for (Row row : pgRowSet) {
                        list.add(from(row));
                    }
                    return list;
                });
    }

    public static Uni<List<Comment>> findByUsername(PgPool client, String username) {
        return client.preparedQuery("SELECT comment_id, sample_id, username, content FROM socialservice.comments WHERE username = $1",  Tuple.of(username))
                .onItem().apply(pgRowSet -> {
                    List<Comment> list = new ArrayList<>(pgRowSet.size());
                    for (Row row : pgRowSet) {
                        list.add(from(row));
                    }
                    return list;
                });
    }

    public Uni<Long> save(PgPool client) {
        return client.preparedQuery("INSERT INTO socialservice.comments (sample_id, username, content) " +
                "VALUES ($1, $2, $3) RETURNING (comment_id)", Tuple.of(sampleId, username, content))
                .onItem().apply(pgRowSet -> pgRowSet.iterator().next().getLong("id"));
    }

    public Uni<Boolean> update(PgPool client) {
        return client.preparedQuery("UPDATE socialservice.comments SET content = $1 WHERE comment_id = $2", Tuple.of(content, id))
                .onItem().apply(pgRowSet -> pgRowSet.rowCount() == 1);
    }

    public static Uni<Boolean> delete(PgPool client, Long id) {
        return client.preparedQuery("DELETE FROM socialservice.comments WHERE comment_id = $1", Tuple.of(id))
                .onItem().apply(pgRowSet -> pgRowSet.rowCount() == 1);
    }

    private static Comment from(Row row)  {
        return new Comment(row.getLong("comment_id"),
                row.getString("username"), row.getString("content"), row.getLong("sample_id"));
    }

}