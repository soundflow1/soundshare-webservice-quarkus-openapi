package org.acme.rest.client;

import org.jboss.resteasy.annotations.providers.multipart.PartType;

import javax.ws.rs.FormParam;
import javax.ws.rs.core.MediaType;

public class MultipartBody {


    @FormParam("sample_id")
    @PartType(MediaType.TEXT_PLAIN)
    public Long sampleId;

    @FormParam("username")
    @PartType(MediaType.TEXT_PLAIN)
    public String username;

    @FormParam("content")
    @PartType(MediaType.TEXT_PLAIN)
    public String content;

}