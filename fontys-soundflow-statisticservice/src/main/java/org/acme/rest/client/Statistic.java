package org.acme.rest.client;

import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.Tuple;
import org.eclipse.microprofile.faulttolerance.CircuitBreaker;

import java.util.ArrayList;
import java.util.List;

public class Statistic {

    public Long id;
    public Long sample_id;
    public Long like_count;
    public Long play_count;

    public Statistic() {

    }

    public Statistic(Long id, Long sample_id, Long like_count, Long play_count) {
        this.id = id;
        this.sample_id = sample_id;
        this.like_count = like_count;
        this.play_count = play_count;
    }

    public Statistic(Long sample_id, Long like_count, Long play_count) {
        this.sample_id = sample_id;
        this.like_count = like_count;
        this.play_count = play_count;
    }

    public Statistic(Long sample_id) {
        this.sample_id = sample_id;
    }

    @CircuitBreaker(requestVolumeThreshold = 4)
    public static Uni<List<Statistic>> findAll(PgPool client) {
        return client.query("select statistic_id , sample_id, like_count, play_count from socialservice.statistics")
                .onItem().apply(pgRowSet -> {
                    List<Statistic> list = new ArrayList<>(pgRowSet.size());
                    for (Row row : pgRowSet) {
                        list.add(from(row));
                    }
                    return list;
                });
    }
    @CircuitBreaker(requestVolumeThreshold = 4)
    public static Uni<List<Statistic>> findById(PgPool client, Long id) {
        return client.preparedQuery("SELECT statistic_id, sample_id, like_count, play_count FROM socialservice.statistics WHERE statistic_id = $1", Tuple.of(id))
                .onItem().apply(pgRowSet -> {
                    List<Statistic> list = new ArrayList<>(pgRowSet.size());
                    for (Row row : pgRowSet) {
                        list.add(from(row));
                    }
                    return list;
                });
    }

    @CircuitBreaker(requestVolumeThreshold = 4)
    public static Uni<List<Statistic>> findBySampleId(PgPool client, Long id) {
        return client.preparedQuery("SELECT statistic_id, sample_id, like_count, play_count FROM socialservice.statistics WHERE sample_id = $1", Tuple.of(id))
                .onItem().apply(pgRowSet -> {
                    List<Statistic> list = new ArrayList<>(pgRowSet.size());
                    for (Row row : pgRowSet) {
                        list.add(from(row));
                    }
                    return list;
                });
    }

    @CircuitBreaker(requestVolumeThreshold = 4)
    public Uni<Long> save(PgPool client) {
        return client.preparedQuery("INSERT INTO socialservice.statistics (sample_id, like_count, play_count) " +
                "VALUES ($1, $2, $3) RETURNING (statistic_id)", Tuple.of(sample_id, like_count, play_count))
                .onItem().apply(pgRowSet -> pgRowSet.iterator().next().getLong("id"));
    }

    @CircuitBreaker(requestVolumeThreshold = 4)
    public Uni<Boolean> update(PgPool client) {
        return client.preparedQuery("UPDATE socialservice.statistics SET like_count = $1, play_count = $2 WHERE statistic_id = $3", Tuple.of(like_count, play_count, id))
                .onItem().apply(pgRowSet -> pgRowSet.rowCount() == 1);
    }
    // https://stackoverflow.com/questions/36378646/on-insert-column-reference-score-is-ambiguous
    @CircuitBreaker(requestVolumeThreshold = 4)
    public Uni<Boolean> updateLikes(PgPool client) {
        return client.preparedQuery("INSERT INTO socialservice.statistics (sample_id, like_count, play_count)" +
                "VALUES ($1, 1, 1) ON CONFLICT (sample_id) DO UPDATE SET like_count = socialservice.statistics.like_count + 1 RETURNING (statistic_id)", Tuple.of(sample_id))
                .onItem().apply(pgRowSet -> pgRowSet.rowCount() == 1);
    }

    @CircuitBreaker(requestVolumeThreshold = 4)
    public Uni<Boolean> updatePlays(PgPool client) {
        return client.preparedQuery("INSERT INTO socialservice.statistics (sample_id, play_count, like_count)" +
                "VALUES ($1, 1, 1) ON CONFLICT (sample_id) DO UPDATE SET play_count = socialservice.statistics.play_count + 1 RETURNING (statistic_id)", Tuple.of(sample_id))
                .onItem().apply(pgRowSet -> pgRowSet.rowCount() == 1);
    }

    @CircuitBreaker(requestVolumeThreshold = 4)
    public static Uni<Boolean> delete(PgPool client, Long id) {
        return client.preparedQuery("DELETE FROM socialservice.statistics WHERE statistic_id = $1", Tuple.of(id))
                .onItem().apply(pgRowSet -> pgRowSet.rowCount() == 1);
    }

    private static Statistic from(Row row)  {
        return new Statistic(row.getLong("statistic_id"),
                row.getLong("sample_id"), row.getLong("like_count"), row.getLong("play_count"));
    }

}