package org.acme.rest.client;

import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import org.eclipse.microprofile.faulttolerance.Retry;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/statistics")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class StatisticResource {

    @Inject
    PgPool client;

    @GET
    @Retry(maxRetries = 4)
    public Uni<Response> get() {
        return Statistic.findAll(client)
                .onItem().apply(Response::ok)
                .onItem().apply(Response.ResponseBuilder::build);
    }

    @GET
    @Path("{id}")
    @Retry(maxRetries = 4)
    public Uni<Response> getSingle(@PathParam Long id) {
        return Statistic.findById(client, id)
                .onItem().apply(comment -> comment != null ? Response.ok(comment) : Response.status(Response.Status.NOT_FOUND))
                .onItem().apply(Response.ResponseBuilder::build);
    }

    @GET
    @Path("/bysample/{id}")
    @Retry(maxRetries = 4)
    public Uni<Response> getSingleBySample(@PathParam Long id) {
        return Statistic.findBySampleId(client, id)
                .onItem().apply(comment -> comment != null ? Response.ok(comment) : Response.status(Response.Status.NOT_FOUND))
                .onItem().apply(Response.ResponseBuilder::build);
    }
    // error
    @POST
    @Path("/likes/{sampleid}")
    @Retry(maxRetries = 4)
    public Uni<Response> updateLikes(@PathParam Long sampleid) {
        Statistic c = new Statistic(sampleid);
        return c.updateLikes(client)
                .onItem().apply(id -> URI.create("/statistics/" + id))
                .onItem().apply(uri -> Response.created(uri).build());
    }

    @POST
    @Path("/plays/{sampleid}")
    @Retry(maxRetries = 4)
    public Uni<Response> updatePlays(@PathParam Long sampleid) {
        Statistic c = new Statistic(sampleid);
        return c.updatePlays(client)
                .onItem().apply(id -> URI.create("/statistics/" + id))
                .onItem().apply(uri -> Response.created(uri).build());
    }

    @PUT
    @Path("{id}")
    @Retry(maxRetries = 4)
    public Uni<Response> update(@PathParam Long id, Statistic comment) {
        return comment.update(client)
                .onItem().apply(updated -> updated ? Response.Status.OK : Response.Status.NOT_FOUND)
                .onItem().apply(status -> Response.status(status).build());
    }

    @DELETE
    @Path("{id}")
    @Retry(maxRetries = 4)
    public Uni<Response> delete(@PathParam Long id) {
        return Statistic.delete(client, id)
                .onItem().apply(deleted -> deleted ? Response.Status.NO_CONTENT : Response.Status.NOT_FOUND)
                .onItem().apply(status -> Response.status(status).build());
    }
}
